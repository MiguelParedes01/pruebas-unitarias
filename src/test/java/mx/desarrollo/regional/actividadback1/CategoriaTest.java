package mx.desarrollo.regional.actividadback1;

import mx.desarrollo.regional.actividadback1.entity.Categoria;
import mx.desarrollo.regional.actividadback1.service.CategoriaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertNotNull;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest
public class CategoriaTest {

    private final CategoriaService categoriaService;

    private Categoria categoria;
    @Autowired
    public CategoriaTest(CategoriaService categoriaService) {
        this.categoriaService = categoriaService;
    }

    @Test
    public void findAllCategoria() throws Exception {
        assertNotNull(categoriaService.findAll());
    }


    @Test
    public void findById() throws Exception {
        final Long id = 2L;
        assertNotNull(categoriaService.findById(id));
    }


    @Test
    public void deleteCategoria() throws Exception {
        final Long id = 1L;
        assert (categoriaService.delete(id));
    }


    @Test
    public void saveCategoria() throws Exception {
        categoria = new Categoria();
        categoria.setDescripcion("Description");
        categoria.setEstado(true);
        categoria.setNombre("Name");
        assertNotNull(categoriaService.save(categoria));
    }


    @Test
    public void updateCategoria() throws Exception {
        categoria = new Categoria();
        categoria.setId_categoria(1L);
        categoria.setDescripcion("Description");
        categoria.setEstado(true);
        categoria.setNombre("Name");
        assertNotNull(categoriaService.save(categoria));
    }
}

package mx.desarrollo.regional.actividadback1;

import mx.desarrollo.regional.actividadback1.entity.Categoria;
import mx.desarrollo.regional.actividadback1.entity.Producto;
import mx.desarrollo.regional.actividadback1.service.ProductoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;


@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest
public class ProductoTest {
    private final ProductoService productoService;

    private Producto producto;

    @Autowired
    public ProductoTest(ProductoService productoService) {
        this.productoService = productoService;
    }

    @Test
    public void findAllProducts() throws Exception {
        assertNotNull(productoService.findAll());
    }


    @Test
    public void findById() throws Exception {
        final Long id = 10L;
        assertNotNull(productoService.findById(id));
    }

    @Test
    public void updateProducto() throws Exception {
        producto = new Producto();
        producto.setId_producto(3L);
        producto.setDescripcion("DESCRIPTION PRODUCT");
        producto.setFechaRegistro(new Date());
        producto.setEstado(true);
        producto.setExistencia(1);
        producto.setPrecio(29.0);
        Categoria categoria = new Categoria();
        categoria.setId_categoria(1L);
        producto.setMarca(categoria);
        productoService.save(producto);
        assertNotNull(productoService.save(producto));
        assertNotNull(productoService.findAll());
    }

    @Test
    public void patchProducto() throws Exception {
        final Long id = 2L;
        assert(productoService.patch(id));
    }

    @Test
    public void deleteProducto() throws Exception {
        final Long id = 1L;
        assert(productoService.patch(id));
    }

    @Test
    public void saveProducto() throws Exception {
        producto = new Producto();
        producto.setDescripcion("DESCRIPTION PRODUCT");
        producto.setFechaRegistro(new Date());
        producto.setEstado(true);
        producto.setExistencia(1);
        producto.setPrecio(29.0);
        Categoria categoria = new Categoria();
        categoria.setId_categoria(1L);
        producto.setMarca(categoria);
        productoService.save(producto);
        assertNotNull(productoService.save(producto));
    }
}

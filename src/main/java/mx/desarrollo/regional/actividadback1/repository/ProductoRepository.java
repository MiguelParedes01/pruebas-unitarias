package mx.desarrollo.regional.actividadback1.repository;

import mx.desarrollo.regional.actividadback1.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {
}
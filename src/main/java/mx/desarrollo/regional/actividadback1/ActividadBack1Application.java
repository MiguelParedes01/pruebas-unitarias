package mx.desarrollo.regional.actividadback1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActividadBack1Application {

    public static void main(String[] args) {
        SpringApplication.run(ActividadBack1Application.class, args);
    }

}

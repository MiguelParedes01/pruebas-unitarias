package mx.desarrollo.regional.actividadback1.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import mx.desarrollo.regional.actividadback1.entity.Producto;
import mx.desarrollo.regional.actividadback1.service.ProductoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/producto/")
public class ProductoController {
    private final ProductoService productoService;

    @Operation(summary = "CONSULTA TODOS LOS PRODUCOTOS ", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @GetMapping("all")
    public List<Producto> findAll(){
        return productoService.findAll();
    }

    @Operation(summary = "CONSULTA UN PRODUCTO POR ID", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @GetMapping("{id}")
    public Optional<Producto> findById(@PathVariable Long id){
        return productoService.findById(id);
    }

    @Operation(summary = "CREA UN NUEVO PRODUCTO", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @PostMapping("")
    public Producto save(@RequestBody Producto producto){
        return productoService.save(producto);
    }

    @Operation(summary = "ACTUALIZA UN PRODUCTO", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @PutMapping("")
    public Producto update(@RequestBody Producto producto){
        return productoService.update(producto);
    }

    @Operation(summary = "ACTUALIZA PARCIALMENTE UN PRODUCTO", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @PatchMapping("{id}")
    public boolean patch(@PathVariable Long id){
        return productoService.patch(id);
    }

    @Operation(summary = "ELIMINA UN PRODUCTO", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @DeleteMapping("{id}")
    public boolean delete(@PathVariable Long id){
        return productoService.delete(id);
    }
}

package mx.desarrollo.regional.actividadback1.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import mx.desarrollo.regional.actividadback1.entity.Categoria;
import mx.desarrollo.regional.actividadback1.service.CategoriaService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/categoria/")
public class CategoriaController {
    private final CategoriaService categoriaService;

    @Operation(summary = "CONSULTA UNA LISTA DE CATEGORIAS", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @GetMapping("all")
    public List<Categoria> findAll(){
        return categoriaService.findAll();
    }
    @Operation(summary = "CONSULTA UNA CATEGORIA POR ID", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @GetMapping("{id}")
    public Optional<Categoria> findById(@PathVariable Long id){
        return categoriaService.findById(id);
    }
    @Operation(summary = "CREA UNA NUEVA CATEGORIA", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @PostMapping("")
    public Categoria save(@RequestBody Categoria categoria){
        return categoriaService.save(categoria);
    }

    @Operation(summary = "ACTUALIZA UNA CATEGORIA", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @PutMapping("")
    public Categoria update(@RequestBody Categoria categoria){
        return categoriaService.update(categoria);
    }

    @Operation(summary = "ACTUALIZA PARCIALMENTE UNA CATEGORIA", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @PatchMapping("{id}")
    public boolean patch(@PathVariable Long id){
        return categoriaService.patch(id);
    }

    @Operation(summary = "ELIMINA UNA CATEGORIA", responses = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = @Content()),
            @ApiResponse(responseCode = "400", description = "Error",
                    content = @Content())
    })
    @DeleteMapping("{id}")
    public boolean delete(@PathVariable Long id){
        return categoriaService.delete(id);
    }

}

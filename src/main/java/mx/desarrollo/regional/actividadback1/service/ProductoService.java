package mx.desarrollo.regional.actividadback1.service;

import lombok.AllArgsConstructor;
import mx.desarrollo.regional.actividadback1.entity.Producto;
import mx.desarrollo.regional.actividadback1.repository.ProductoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class ProductoService {
    private final ProductoRepository productoRepository;

    public List<Producto> findAll(){
       return productoRepository.findAll();
    }

    public Optional<Producto> findById(Long id){
        return productoRepository.findById(id);
    }

    public Producto save(Producto producto){
        return productoRepository.save(producto);
    }

    public boolean delete(Long id){
        if(productoRepository.existsById(id)){
            productoRepository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }

    public boolean patch(Long id){
        Optional<Producto> optionalProducto = productoRepository.findById(id);
        if(optionalProducto.isPresent() && optionalProducto.get().isEstado()){
            Producto producto = optionalProducto.get();
            producto.setEstado(false);
            productoRepository.save(producto);
            return true;
        }else{
            return false;
        }
    }

    public Producto update(Producto producto) {
        if (productoRepository.existsById(producto.getId_producto())){
            return productoRepository.save(producto);
        }else {
            return null;
        }
    }
}

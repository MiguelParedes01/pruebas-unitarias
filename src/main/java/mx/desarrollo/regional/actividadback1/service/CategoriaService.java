package mx.desarrollo.regional.actividadback1.service;

import lombok.AllArgsConstructor;
import mx.desarrollo.regional.actividadback1.entity.Categoria;
import mx.desarrollo.regional.actividadback1.repository.CategoriaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class CategoriaService {

    private final CategoriaRepository categoriaRepository;

    public List<Categoria> findAll(){
        return categoriaRepository.findAll();
    }

    public Optional<Categoria> findById(Long id){
        return categoriaRepository.findById(id);
    }

    public Categoria save(Categoria categoria){
        return categoriaRepository.save(categoria);
    }

    public Categoria update(Categoria categoria){
        if (categoriaRepository.existsById(categoria.getId_categoria())){
            return categoriaRepository.save(categoria);
        }else {
            return null;
        }
    }

    public boolean delete(Long id){
        if(categoriaRepository.existsById(id)){
            categoriaRepository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }

    public boolean patch(Long id){
        Optional<Categoria> optionalCategoria = categoriaRepository.findById(id);
        if(optionalCategoria.isPresent()){
            Categoria categoria = optionalCategoria.get();
            categoria.setEstado(false);
            categoriaRepository.save(categoria);
            return true;
        }else{
            return false;
        }
    }

}
